const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist/webpack')
  },
  module: {
    rules: [
      //{ test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
      { test: /\.css$/, use: [ MiniCssExtractPlugin.loader, "css-loader"] },
      { test: /\.(png|svg|jpg|gif)$/, use: [ 'file-loader'] },
      { test: /\.(woff|woff2|eot|ttf|otf)$/, use: [ 'file-loader'] },
      { test: /\.js$/, use: [ "babel-loader" ], exclude: /node_modules/ },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
      inject: 'body'
    })
  ]
};
