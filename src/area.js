function area(width, height) {
  return Math.abs(width * height);
}

module.exports = area;
