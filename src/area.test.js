import area from './area.js'

test('aire 10x20 donne 200', () => {
  expect(area(10, 20)).toBe(200);
})

test('aire est toujours positive', () => {
  expect(area(-10, 20)).toBeGreaterThan(0);
});
